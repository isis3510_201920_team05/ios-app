//
//  LogInViewController.swift
//  huestyle
//
//  Created by Juan Pablo Gonzalez P on 10/6/19.
//  Copyright © 2019 Juan Pablo Gonzalez P. All rights reserved.
//

import UIKit
import Firebase
import SVProgressHUD

class LogInViewController: UIViewController {
    
    @IBOutlet weak var emailTextField: UITextField!
    
    @IBOutlet weak var passwordTextField: UITextField!
    
    @IBOutlet weak var logInButton: UIButton!
    
    @IBOutlet weak var errorMessageLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        setUpElements()
    }

    func setUpElements() {
        // Hide the error label
        errorMessageLabel.alpha = 0
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func logInTapped(_ sender: Any) {
        
        
        //SVProgressHUD.show()
        

        // TODO: Validate Text Fields
        // Signing in the user
        Auth.auth().signIn(withEmail: emailTextField.text!.trimmingCharacters(in: .whitespacesAndNewlines), password: passwordTextField.text!.trimmingCharacters(in: .whitespacesAndNewlines)) { (user, error) in
            if error != nil {
                // Couldn't sign in
                print("no pudo iniciar")
                self.errorMessageLabel.text = error!.localizedDescription
                self.errorMessageLabel.alpha = 1
                //SVProgressHUD.dismiss()
            }
            else {
                print("Log in successful!")
                
                //SVProgressHUD.dismiss()

                self.performSegue(withIdentifier: "goToHome", sender: self)
                //let homeViewController = self.storyboard?.instantiateViewController(identifier: Constants.Storyboard.HomeViewController) as? HomeViewController
                
                //self.view.window?.rootViewController = homeViewController
                //self.view.window?.makeKeyAndVisible()
            }
        }
    }
    
}
